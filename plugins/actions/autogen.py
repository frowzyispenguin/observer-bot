# -*- coding: utf-8 -*-

"""generate an automatic response using files located in /responses/autogen"""

__author__ = "Hadi Azami"

from os import listdir
from os.path import isfile, join, abspath, dirname

from pyrogram import Client, Filters

from helpers.filters import reply_to_bot


def get_commands():
    path = dirname(abspath(__file__))
    path = path.replace("/plugins/actions", "/responses/autogen")
    result = dict()

    def fmt(f):
        """ format file name into command """
        return "!"+f.replace(".md", "")

    files = [f for f in listdir(path) if isfile(join(path, f))]
    for f in files:
        with open(join(path, f), encoding='utf-8') as reader:
            content = reader.read()
        result.update({
            fmt(f): content
        })
    return result


commands = get_commands()


def is_autogen(filter, update):
    message = update.text.split()
    return True if commands.get(message[0]) else False


@Client.on_message(
    ~ Filters.create(reply_to_bot, "ReplyToBot")
    & Filters.text
    & Filters.create(func=is_autogen, name="IsAutogen")
)
def autogen(client, message):
    key = message.text.split()[0]

    reply_to = None
    if message.reply_to_message:
        reply_to = message.reply_to_message.message_id

    message.reply_text(
        text=commands.get(key),
        reply_to_message_id=reply_to,
        quote=not bool(reply_to)
    )
