# -*- coding: utf-8 -*-

"""update chat record on migration"""

__author__ = "Hadi Azami"


from pyrogram import Client, Filters

from data import chat


@Client.on_message(
    Filters.migrate_to_chat_id
    & Filters.group
)
def migration(client, message):
    chat.migrate(message.chat.id, message.migrate_to_chat_id)
