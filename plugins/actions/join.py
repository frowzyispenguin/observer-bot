# -*- coding: utf-8 -*-

""" Actions related to join """

__author__ = "Hadi Azami"

from pyrogram import Client, Filters

from helpers.cfg import Config
from helpers.functions import private_message
from data import chat


def bot_joined(filter, update):
    for user in update.new_chat_members:
        if user.username == Config.bot_username:
            return True
    return False


@Client.on_message(
    Filters.new_chat_members
    & Filters.create(bot_joined)
    & Filters.group
)
def add_group(client, message):
    """ Add group to database when somone joins the bot """
    chat.add(message.chat.id, message.from_user.id)

    reply = Config.responses.get("add_group").format(
        message.chat.title,
        message.chat.id
    )

    private_message(
        client=client,
        chat_id=message.from_user.id,
        text=reply
    )
