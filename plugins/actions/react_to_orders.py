# -*- coding: utf-8 -*-

"""does an action based on orders received from admins via callback query"""

__author__ = "Hadi Azami"

from pyrogram import Client

from helpers.cfg import Config
from helpers.functions import admin, check_permissions


@Client.on_callback_query()
@check_permissions
def react(client, query):
    """ order pattern: order_type,group_id,message_id
        eg: delete,12345,67891011
    """
    pattern = query.data.split(',')
    group_id = int(pattern[1])
    invoced_by = query.from_user.id

    if not admin(client, group_id, invoced_by):
        err = "شما دسترسی لازم برای انجام این عملیات را ندارید."
        query.answer(err, show_alert=True)
        return

    if pattern[0] == 'delete':
        res = client.delete_messages(int(pattern[1]), int(pattern[2]))
        if res:
            first_name = query.from_user.first_name
            text = query.message.text.markdown

            reply = Config.responses.get("delete")
            reply = reply.format(text)
            reply = reply.replace("first_name", first_name)
            reply = reply.replace("link", "tg://user?id={}".format(invoced_by))

            client.edit_message_text(
                chat_id=query.message.chat.id,
                message_id=query.message.message_id,
                text=reply,
                parse_mode="markdown",
                reply_markup=None
            )

            query.answer("پیام حذف شد")
        else:
            query.answer("خطا در انجام عملیات", show_alert=True)
