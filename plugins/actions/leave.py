# -*- coding: utf-8 -*-

""" Actions related to leave """

__author__ = "Hadi Azami"

from pyrogram import Client, Filters

from helpers.cfg import Config
from helpers.functions import private_message
from data import chat


def bot_kicked(message):
    username = message.left_chat_member.username
    return True if username == Config.bot_username else False


def owner_left(message):
    group = chat.get(message.chat.id)
    return True if group.owner == message.left_chat_member.id else False


@Client.on_message(
    Filters.left_chat_member
)
def leave(client, message):
    if owner_left(message):
        client.leave_chat(message.chat.id)
        chat.delete(message.chat.id)
    elif bot_kicked(message):
        chat_info = chat.get(message.chat.id)
        reply = Config.responses.get("delete_group").format(
            message.chat.title
        )
        private_message(
            client=client,
            chat_id=chat_info.owner,
            text=reply
        )
        chat.delete(message.chat.id)
