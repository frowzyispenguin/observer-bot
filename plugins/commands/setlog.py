# -*- coding: utf-8 -*-

# command: /setlog
# region: channel
# description: Setting a log channel (/setlog GROUP_ID)

""" Set current channel as log channel for given group """

__author__ = "Hadi Azami"

from pyrogram import Client, Filters

from data import chat


@Client.on_message(
    Filters.channel
    & Filters.command('setlog', "/")
)
def setlog(client, message):
    args = message.text.split(" ")

    try:
        group = chat.get(int(args[1]))
    except (IndexError, ValueError):
        return

    if group is not None:
        chat.setlog(group, message.chat.id)
        message.reply_text("عملیات انجام شد.", quote=False)
    else:
        message.reply_text(
            text="گروه موردنظر یافت نشد.",
            quote=False
        )
