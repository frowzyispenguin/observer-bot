# -*- coding: utf-8 -*-

# command: !dgg
# alias: !ddg
# region: group
# description: Let me DuckDuckGo that for you (!dgg hello world)

""" generate duckduckgo.com link for people who don't know how to search """

__author__ = "abtinmo, Hadi Azami, Nima HeydariNasab"

import html
import urllib.parse

from pyrogram import Client, Filters
from pyrogram.errors import MessageDeleteForbidden


from helpers.cfg import Config
from helpers.filters import reply_to_bot


@Client.on_message(
    Filters.command(commands=["dgg", "ddg"], prefix="!")
    & ~ Filters.create(reply_to_bot)
    & Filters.group
)
def link(client, message):
    """ Main function. Handle !dgg command. """

    text = message.text.split(" ")[1:]

    # delete empty queries
    if len(text) == 0:
        try:
            message.delete()
            return
        except MessageDeleteForbidden:
            return

    # replay the message, if command replayed to a message
    reply_to = None
    if message.reply_to_message:
        reply_to = message.reply_to_message.message_id

    # create the link
    link = "https://dgg.gg/?q=" + urllib.parse.quote_plus(" ".join(text))
    response = Config.responses.get('dgg')
    response = response.replace(
        "query",
        html.escape(" ".join(text))).replace("link", link)
    message.reply_text(
        text=response,
        reply_to_message_id=reply_to,
        quote=not bool(reply_to)
    )
