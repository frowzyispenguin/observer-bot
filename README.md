# Observer Bot

## READ BEFORE DOING ANYTHING:
Project structure: https://gitlab.com/linkosi/observer-bot/issues/2#note_205050015

How should we commit: https://gitlab.com/linkosi/observer-bot/issues/2#note_205075286

Code of conduct: https://gitlab.com/linkosi/observer-bot/issues/2#note_205079538

How to submit an issue: https://gitlab.com/linkosi/observer-bot/issues/2#note_205081794

Work in progress documentation (always visit this link): https://gitlab.com/linkosi/observer-bot/issues/7

Pyrogram documentation: https://docs.pyrogram.org

## Configuration
You need API Keys: https://docs.pyrogram.org/intro/setup#api-keys

Also a bot token so you have to register a bot via @botfather on telegram


Create a file called `config.ini` placed at the root of your working directory. (this file *MUST* be gitignored)

Note: All values are treated as **string**

```ini
[pyrogram]
api_id = 123456
api_hash = 2b578b301609ce6b0ed5d303f
bot_token = 866088169:AAETL9cneYqQVdh3pe6fy2

[settings]
discard_message_timeout=10
bot_username = DemoObserverBot
```

## Development
* **Linter**: flake8
* **Tests**: pytest

### File headers
```
# -*- coding: utf-8 -*-
__author__ = ""
```

### Make options

```
clean                Remove all build, test and Python artifacts.
clean-build          Remove build artifacts.
clean-pyc            Remove Python file artifacts.
clean-test           Remove test.
lint                 Check style with flake8.
test                 Run tests quickly with the default Python.
test-all             Run tests on every Python version with tox.
release              Package and upload a release.
dist                 Builds source and wheel package.
install              Install the package to the active Python's site-packages.
```


### Prerequisites
`python3 -m venv env && source env/bin/activate && pip install -U -r requirements_dev.txt && pip install -U -r requirements.txt`


### Docker

You can also run this project using docker/docker-compose.

An alpine image is used for the sake of simplicity.


```.bash
docker-compose up bot
```
