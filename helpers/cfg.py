# -*- coding: utf-8 -*-
__author__ = "Hadi Azami"

import configparser
from os import listdir
from os.path import isfile, join, abspath, dirname

config = configparser.ConfigParser()
path = dirname(abspath(__file__))
path = path.replace("/helpers", "")
config.read(join(path, "config.ini"))


def cache_responses():
    path = dirname(abspath(__file__))
    path = path.replace("/helpers", "/responses")
    result = dict()

    def fmt(f):
        """ format file name into command """
        return f.replace(".md", "")

    files = [f for f in listdir(path) if isfile(join(path, f))]
    for f in files:
        with open(join(path, f), encoding='utf-8') as reader:
            content = reader.read()
        result.update({
            fmt(f): content
        })
    return result


class Config(object):
    responses = cache_responses()
    bot_token = config["pyrogram"]["bot_token"]
    discard_timeout = int(config["settings"]["discard_message_timeout"])
    bot_username = config["settings"]["bot_username"]
