from setuptools import setup, find_packages
from codecs import open
from os import path

__version__ = '0.0.1'

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

# get the dependencies and installs
with open(path.join(here, 'requirements.txt'), encoding='utf-8') as f:
    all_reqs = f.read().split('\n')

install_requires = [x.strip() for x in all_reqs if 'git+' not in x]
dependency_links = [x.strip().replace('git+', '') for x in all_reqs if x.startswith('git+')]

setup_requirements = ['pytest-runner']
test_requirements = ['pytest']


setup(
    name='observerbot',
    version=__version__,
    description='a Telegram bot that observe your group and help admins to manage easier. ',
    long_description=long_description,
    url='__FINAL_URL_HERE',
    download_url='PATH_TO_GITHUB/tarball/' + __version__,
    license='AGPL',
    classifiers=[
      'Development Status :: 3 - Alpha',
      'Intended Audience :: Developers',
      'Programming Language :: Python :: 3.5',
      'Programming Language :: Python :: 3.6',
      'Programming Language :: Python :: 3.7',
    ],
    keywords='',
    packages=find_packages(exclude=['docs', 'tests*']),
    include_package_data=True,
    author='',
    install_requires=install_requires,
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    dependency_links=dependency_links,
    author_email=''
)
