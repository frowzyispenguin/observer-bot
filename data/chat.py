# -*- coding: utf-8 -*-
__author__ = "Hadi Azami"

import mongoengine
from typing import List


class Chat(mongoengine.Document):
    chat_id = mongoengine.LongField(primary_key=True)
    owner = mongoengine.IntField(required=True)
    log_channel = mongoengine.LongField(default=0)
    migrated_from = mongoengine.LongField(default=0)

    meta = {
        "db_alias": "bot",
        "collection": "chats"
    }


def add(chat_id, owner):
    """ Register a group chat """
    chat = Chat()
    chat.chat_id = chat_id
    chat.owner = owner
    chat.save()


def delete(chat_id) -> bool:
    """ Delete a group chat """
    chat = Chat.objects(chat_id=chat_id).first()
    if chat is None:
        return False
    chat.delete()
    return True


def migrate(from_chat, to_chat):
    """ Update a chat record on migration """
    old_chat = Chat.objects(chat_id=from_chat).first()

    new_chat = Chat()
    new_chat.chat_id = to_chat
    new_chat.owner = old_chat.owner
    new_chat.log_channel = old_chat.log_channel
    new_chat.migrated_from = from_chat
    new_chat.save()

    old_chat.delete()


def get(chat_id) -> Chat:
    """ Get chat info """
    return Chat.objects(chat_id=chat_id).first()


def get_chats(owner_id) -> List[Chat]:
    """ Get all chats registered by owner """
    return list(Chat.objects(owner=owner_id))


def setlog(chat, channel_id):
    """ Add a log channel to a chat """
    chat.log_channel = channel_id
    chat.save()
